package rental;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public interface ICarRentalCompany extends Remote {
	public Set<CarType> getAvailableCarTypes(Date from, Date end) throws RemoteException;
	public Quote createQuote (ReservationConstraints resCon, String client) throws RemoteException, ReservationException;
	public Reservation confirmQuote (Quote quote) throws RemoteException, ReservationException;
	public List<Reservation> getAllReservations (String renter) throws RemoteException;
	public int getNumberOfReservationsForCarType (String carType) throws RemoteException;
	public List<CarType> getAllCarTypes () throws RemoteException;
	public List<Reservation> getAllReservations() throws RemoteException;
	public CarType getMostPopularCarType(int year) throws RemoteException;
	public void cancelReservation(Reservation res) throws RemoteException;
	public List<String> getRegions() throws RemoteException;

}

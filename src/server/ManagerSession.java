package server;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import rental.CarType;
import session.IManagerSession;

/**
 * @author Maarten en Axel
 * Stateless session!
 *
 */
public class ManagerSession implements IManagerSession {
	
	private SessionManager sessionManager;
	
	ManagerSession(SessionManager sessionManager){
		this.sessionManager = sessionManager;
	}

	@Override
	public void registerCompany(String company) throws RemoteException {
		this.sessionManager.getAgency().registerCompany(company);
	}

	@Override
	public void unregisterComany(String company) {
		this.sessionManager.getAgency().unregisterCompany(company);
	}

	@Override
	public List<String> getRegisteredCompanies() {
		return this.sessionManager.getAgency().getRegisteredCompanies();
	}

	@Override
	public List<CarType> getCarTypes(String company) throws RemoteException {
		return this.sessionManager.getAgency().getCarTypes(company);
	}

	@Override
	public int nbOfReservations(String company, String carType) throws RemoteException {
		return this.sessionManager.getAgency().nbOfReservations(company, carType);
	}

	@Override
	public Set<String> getBestClients() throws RemoteException {
		return this.sessionManager.getAgency().getBestClients();
	}

	@Override
	public CarType getMostPopularCarType(String company, int year) throws RemoteException {
		return this.sessionManager.getAgency().getMostPopularCarType(company, year);
	}

}

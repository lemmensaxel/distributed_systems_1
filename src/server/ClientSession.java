package server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rental.Quote;
import rental.Reservation;
import rental.ReservationException;
import session.IClientSession;

public class ClientSession implements IClientSession {
	
	private SessionManager sessionManager;
	private String client;
	private List<Quote>  quotes;
	
	ClientSession(SessionManager sessionManager, String client) {
		this.sessionManager = sessionManager;
		this.client = client;
		this.quotes = new ArrayList<>();
	}

	@Override
	public Quote addQuote(String client, Date start, Date end, String CarType, String Region) throws RemoteException, ReservationException {
		Quote quote = this.sessionManager.getAgency().createQuote(client, start, end, CarType, Region);
		this.quotes.add(quote);
		return quote;
	}

	@Override
	public List<Quote> getCurrentQuotes() {
		return this.quotes;
	}

	@Override
	public List<Reservation> confirmQuotes() throws RemoteException, ReservationException {
		try {
			return this.sessionManager.getAgency().confirmQuotes(this.quotes);
		} catch (ReservationException e) {
			this.sessionManager.closeSession(this.client);
			throw new ReservationException("Reservation failed!");
		}
	}

	@Override
	public List<String> getAvailableCarTypes(Date start, Date end) throws RemoteException {
		return this.sessionManager.getAgency().getAvailableCarTypes(start, end);
	}

	@Override
	public String getCheapestCarType(Date start, Date end, String region) throws RemoteException {
		return this.sessionManager.getAgency().getCheapestCarType(start, end, region);
	}

}

package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import session.IClientSession;
import session.IManagerSession;
import session.ISessionManager;

public class SessionManager implements ISessionManager{
	
	
	private HashMap<String, ClientSession> clientSessions;
	private RentalAgency agency;
	
	SessionManager(RentalAgency agency){
		this.agency = agency;
		this.clientSessions = new HashMap<>();
	}
	
	RentalAgency getAgency(){
		return this.agency;
	}

	@Override
	public IClientSession getClientSession(String client) throws RemoteException {
		if(this.clientSessions.containsKey(client)){
			return (IClientSession) UnicastRemoteObject.exportObject(this.clientSessions.get(client), 0);
		}else{
			ClientSession newClientSession = new ClientSession(this,client);
			this.clientSessions.put(client, newClientSession);
			return (IClientSession) UnicastRemoteObject.exportObject(newClientSession, 0);
		}
	}

	@Override
	public IManagerSession getManagerSession() throws RemoteException {
		ManagerSession managerSession = new ManagerSession(this);
		return (IManagerSession) UnicastRemoteObject.exportObject(managerSession, 0);
	}
	
	public void closeSession(String client){
		this.clientSessions.remove(client);
	}
	

}

package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rental.Car;
import rental.CarType;
import rental.ICarRentalCompany;
import rental.Quote;
import rental.Reservation;
import rental.ReservationConstraints;
import rental.ReservationException;
import session.ISessionManager;

/**
 * @author axel
 *
 */
public class RentalAgency {
	
	private HashMap<String, ICarRentalCompany> registeredCompanies = new HashMap<>();
	
	public final static String RENTALAGENCY_SESSIONMANAGER = "sessionManager";
	
	public static void main(String[] args) throws RemoteException {
		System.setSecurityManager(null);
		RentalAgency agency = new RentalAgency();
		SessionManager sessionManager = new SessionManager(agency);
		ISessionManager stub = (ISessionManager) UnicastRemoteObject.exportObject(sessionManager, 0);
		Registry registry = LocateRegistry.getRegistry();
		registry.rebind(RentalAgency.RENTALAGENCY_SESSIONMANAGER, stub);
	}
	
	/*
	 * ManagerSession methods
	 */
	
	public List<String> getRegisteredCompanies() {
		List<String> result = new ArrayList<String>();
		result.addAll(this.registeredCompanies.keySet());
		return result;
	}
	
	public void registerCompany(String company) throws RemoteException {
		if(!this.registeredCompanies.containsKey(company.toLowerCase())){
			ICarRentalCompany crc = NameService.getCompany(company.toLowerCase());
			this.registeredCompanies.put(company.toLowerCase(), crc);
		}
	}
	
	public void unregisterCompany(String company) {
		if(this.registeredCompanies.containsKey(company.toLowerCase())){
			this.registeredCompanies.remove(company.toLowerCase());
		}	
	}
	
	public List<CarType> getCarTypes(String company) throws RemoteException {
		return this.registeredCompanies.get(company.toLowerCase()).getAllCarTypes();
	}
	
	public int nbOfReservations(String company, String carType) throws RemoteException {
		return this.registeredCompanies.get(company.toLowerCase()).getNumberOfReservationsForCarType(carType);
	}
	
	public Set<String> getBestClients() throws RemoteException {
	    List<String> customers = new ArrayList<String>();
	    for(ICarRentalCompany comp : this.registeredCompanies.values()){
    		for(Reservation res : comp.getAllReservations()){
    			customers.add(res.getCarRenter());
            }
	    }
	    Set<String> removedDuplicateCustomers = new HashSet<String>();
	    removedDuplicateCustomers.addAll(customers);
	    Set<String> bestCustomers = new HashSet<String>();
	    int amount = 0;
	    for(String cust : removedDuplicateCustomers){
	    	int temp = 0;
	        for(ICarRentalCompany comp : this.registeredCompanies.values()){
	        	temp = temp + comp.getAllReservations(cust).size();
	        }
	        if(temp > amount){
	        	bestCustomers.clear();
	        	bestCustomers.add(cust);
	        	amount = temp;
	        }else if(temp == amount){
	        	bestCustomers.add(cust);
	        }
	    }
	    return bestCustomers;
	}
	
	public CarType getMostPopularCarType(String company, int year) throws RemoteException {
		return this.registeredCompanies.get(company.toLowerCase()).getMostPopularCarType(year);
	}
	
	/*
	 * ClientManager Sessions
	 */
	
	public Quote createQuote(String client, Date start, Date end, String carType, String region) throws RemoteException, ReservationException {
		ReservationConstraints constraints = new ReservationConstraints(start, end, carType, region);
		for(ICarRentalCompany comp: this.registeredCompanies.values()){
			try{
				Quote newQuote = comp.createQuote(constraints, client);
				return newQuote;
			} catch (ReservationException e) {
				
			}
		}
		throw new ReservationException("No Cars of this type available in the given period");
	}
	
	public synchronized List<Reservation> confirmQuotes(List<Quote> quotes) throws RemoteException, ReservationException{
		List<Reservation> reservations = new ArrayList<>();
		for(Quote q: quotes){
			ICarRentalCompany comp = this.registeredCompanies.get(q.getRentalCompany().toLowerCase());
			try {
				Reservation newReservation = comp.confirmQuote(q);
				reservations.add(newReservation);
			} catch (ReservationException e) {
				for(Reservation res : reservations){
					ICarRentalCompany comp2 = this.registeredCompanies.get(res.getRentalCompany().toLowerCase());
					comp2.cancelReservation(res);
				}
				throw new ReservationException("Reservation failed");
			}
		}
		return reservations;
	}
	
	public List<String> getAvailableCarTypes(Date start, Date end) throws RemoteException {
		List<String> carTypes = new ArrayList<>();
		for(ICarRentalCompany comp : this.registeredCompanies.values()){
			for(CarType type : comp.getAvailableCarTypes(start, end)){
				if(!carTypes.contains(type.getName())){
					carTypes.add(type.getName());
				}
			}
		}
		return carTypes;
	}
	
	public String getCheapestCarType(Date start, Date end, String region) throws RemoteException {
		List<CarType> cartypes = new ArrayList<>();
		for(ICarRentalCompany comp : this.registeredCompanies.values()){
			if(comp.getRegions().contains(region)){
				for(CarType type : comp.getAvailableCarTypes(start, end)){
					if(!cartypes.contains(type)){
						cartypes.add(type);
					}
				}
			}
		}
		CarType best = null;
		double amount = cartypes.get(0).getRentalPricePerDay();
		for(CarType type : cartypes){
			if(type.getRentalPricePerDay() < amount){
				best = type;
				amount = type.getRentalPricePerDay();
			}
		}
		return best.getName();
	}
	
}

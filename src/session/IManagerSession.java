package session;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Set;

import rental.CarType;

public interface IManagerSession extends Remote {
	public void registerCompany(String company) throws RemoteException;
	public void unregisterComany(String company) throws RemoteException;
	public List<String> getRegisteredCompanies() throws RemoteException;
	public List<CarType> getCarTypes(String company) throws RemoteException;
	public int nbOfReservations(String company, String CarType) throws RemoteException;
	public Set<String> getBestClients() throws RemoteException;
	public CarType getMostPopularCarType(String Company, int year) throws RemoteException;
	

}

package session;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import rental.Quote;
import rental.Reservation;
import rental.ReservationException;

public interface IClientSession extends Remote {

	public Quote addQuote(String client, Date start, Date end, String CarType, String Region) throws RemoteException, ReservationException;
	public List<Quote> getCurrentQuotes() throws RemoteException;
	public List<Reservation> confirmQuotes() throws RemoteException, ReservationException;
	public List<String> getAvailableCarTypes(Date start, Date end) throws RemoteException;
	public String getCheapestCarType(Date start, Date end, String region) throws RemoteException;
	
}

package session;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISessionManager extends Remote {
	public IClientSession getClientSession(String client) throws RemoteException;
	public IManagerSession getManagerSession() throws RemoteException;
	
}

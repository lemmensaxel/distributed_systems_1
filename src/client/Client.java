package client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Date;
import java.util.List;
import java.util.Set;

import rental.CarType;
import rental.Reservation;
import server.RentalAgency;
import session.IClientSession;
import session.IManagerSession;
import session.ISessionManager;

public class Client extends AbstractTestManagement<IClientSession, IManagerSession> {
	
	/********
	 * MAIN *
	 ********/
	
	public static void main(String[] args) throws Exception {
		System.setSecurityManager(null);
		Client client = new Client("trips");
		client.run();
	}
	
	/***************
	 * CONSTRUCTOR *
	 ***************/
	private Registry reg;
	private ISessionManager sessionManager;
	
	public Client(String scriptFile) throws RemoteException, NotBoundException {
		super(scriptFile);
		this.reg = LocateRegistry.getRegistry("localhost", 1099);
		this.sessionManager = (ISessionManager) this.reg.lookup(RentalAgency.RENTALAGENCY_SESSIONMANAGER);
		//We add these lines because your tests never call registerCompany
		IManagerSession managerSession = this.sessionManager.getManagerSession();
		managerSession.registerCompany("hertz");
		managerSession.registerCompany("dockx");
	}

	@Override
	protected Set<String> getBestClients(IManagerSession ms) throws Exception {
		return ms.getBestClients();
	}

	@Override
	protected String getCheapestCarType(IClientSession session, Date start, Date end, String region) throws Exception {
		return session.getCheapestCarType(start, end, region);
	}

	@Override
	protected CarType getMostPopularCarTypeIn(IManagerSession ms, String carRentalCompanyName, int year)
			throws Exception {
		return ms.getMostPopularCarType(carRentalCompanyName, year);
	}

	@Override
	protected IClientSession getNewReservationSession(String name) throws Exception {
		return this.sessionManager.getClientSession(name);
	}

	@Override
	protected IManagerSession getNewManagerSession(String name, String carRentalName) throws Exception {
		return this.sessionManager.getManagerSession();
	}

	@Override
	protected void checkForAvailableCarTypes(IClientSession session, Date start, Date end) throws Exception {
		session.getAvailableCarTypes(start, end);
	}

	@Override
	protected void addQuoteToSession(IClientSession session, String name, Date start, Date end, String carType,
			String region) throws Exception {
		session.addQuote(name, start, end, carType, region);
		
	}

	@Override
	protected List<Reservation> confirmQuotes(IClientSession session, String name) throws Exception {
		//We don't need the name of the client because the session already knows who his client is
		return session.confirmQuotes();
	}

	@Override
	protected int getNumberOfReservationsForCarType(IManagerSession ms, String carRentalName, String carType)
			throws Exception {
		return ms.nbOfReservations(carRentalName, carType);
	}

		
	
}